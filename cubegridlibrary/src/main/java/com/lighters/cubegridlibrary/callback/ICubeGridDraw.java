package com.lighters.cubegridlibrary.callback;

import ohos.agp.render.Canvas;

/**
 * Created by david on 16/2/5.
 */
public interface ICubeGridDraw {

    /**
     * 绘画单个的小方块
     *
     * @param canvas
     */
    void drawCubeGrid(Canvas canvas);
}
