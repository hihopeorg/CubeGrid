package com.lighters.cubegridlibrary.view;

import com.lighters.cubegridlibrary.callback.ICubeGridAnimCallback;
import com.lighters.cubegridlibrary.model.CubeGridManager;
import com.lighters.cubegridlibrary.model.CubeGridManagerOption;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * Created by david on 16/2/5.
 */
public class CubeGridFrameLayout extends StackLayout implements Component.DrawTask, ComponentContainer.ArrangeListener {

    private CubeGridManager mCubeGridManager;

    private CubeGridManagerOption.Builder mBuilder;

    public CubeGridFrameLayout(Context context) {
        super(context);
    }

    public CubeGridFrameLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        final int loopCount;
        int cornerSize;
        int color;

        if (attrs.getAttr("loopCount").isPresent()) {
            loopCount = attrs.getAttr("loopCount").get().getIntegerValue();
        } else {
            loopCount = 1;
        }
        mBuilder = new CubeGridManagerOption.Builder();
        mBuilder.loopCount(loopCount);
        if (attrs.getAttr("roundCornerSize").isPresent()) {
            cornerSize = attrs.getAttr("roundCornerSize").get().getDimensionValue();
        } else {
            cornerSize = 0;
        }
        mBuilder.cornerSize(cornerSize);
        if (attrs.getAttr("fillColor").isPresent()) {
            color = attrs.getAttr("fillColor").get().getColorValue().getValue();
        } else {
            color = Color.WHITE.getValue();
        }
        mBuilder.fillColor(color);

        // 添加绘制任务
        addDrawTask(this);
        setArrangeListener(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.save();
        getCubeGridManager().drawCanvas(canvas);
        canvas.restore();
    }

    /**
     * 当布局发生改变, 则重新设置宽度跟高度.
     */
    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        if (mBuilder != null) {
            mCubeGridManager = null;
        }
        return false;
    }

    /**
     * 执行开始动画
     */
    public void start() {
        start(null);
    }

    /**
     * 开始执行闪烁的动画
     *
     * @param cubeGridAnimCallback 动画接口回调
     */
    public void start(final ICubeGridAnimCallback cubeGridAnimCallback) {
        new EventHandler(EventRunner.create()).postTask(new Runnable() {
            @Override
            public void run() {
                getCubeGridManager().setCubeGridAnimCallback(cubeGridAnimCallback);
                getCubeGridManager().startLoop(CubeGridFrameLayout.this);
            }
        });
    }

    /**
     * 执行动画的暂停, 但针对每个小方块的动画, 还会完整地执行完成当前的周期
     */
    public void stop() {
        getCubeGridManager().stop();
    }

    /**
     * 执行结束动画
     */
    public void destroy() {
        getCubeGridManager().destroy();
    }

    private CubeGridManager getCubeGridManager() {
        if (mCubeGridManager == null) {
            mCubeGridManager = new CubeGridManager();
            mBuilder.totalWidth(getWidth());
            mBuilder.totalHeight(getHeight());
            mCubeGridManager.setUp(mBuilder.build());
        }
        return mCubeGridManager;
    }
}
