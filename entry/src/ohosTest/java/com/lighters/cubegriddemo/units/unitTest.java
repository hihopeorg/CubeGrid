package com.lighters.cubegriddemo.units;

import com.lighters.cubegridlibrary.model.CubeGridManagerOption;
import com.lighters.cubegridlibrary.model.CubeGridObject;
import org.junit.Assert;
import org.junit.Test;

public class unitTest {

    @Test
    public void getCurLoopCount() {
        int getCurLoopCount = CubeGridObject.getCurLoopCount();

        Assert.assertEquals("It's right", 1, getCurLoopCount);
    }

    @Test
    public void getMaxLoopCount() {
        int getMaxLoopCount = CubeGridObject.getMaxLoopCount();

        Assert.assertEquals("It's right", 2147483647, getMaxLoopCount);
    }

    @Test
    public void getColumnSize() {
        int getColumnSize = CubeGridManagerOption.getColumnSize();

        Assert.assertEquals("It's right", 3, getColumnSize);
    }

    @Test
    public void getRowSize() {
        int getRowSize = CubeGridManagerOption.getRowSize();

        Assert.assertEquals("It's right", 3, getRowSize);
    }

    @Test
    public void getCornerSize() {
        int getCornerSize = CubeGridManagerOption.getCornerSize();

        Assert.assertEquals("It's right", 0, getCornerSize);
    }
}