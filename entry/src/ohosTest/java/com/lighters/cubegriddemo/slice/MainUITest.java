package com.lighters.cubegriddemo.slice;

import com.lighters.cubegriddemo.EventHelper;
import com.lighters.cubegriddemo.MainAbility;
import com.lighters.cubegriddemo.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainUITest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void gotoMainUI() throws InterruptedException {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(5000);
        Button stopTest = (Button) ability.findComponentById(ResourceTable.Id_btn_grid_stop1);
        EventHelper.triggerClickEvent(ability, stopTest);
        Thread.sleep(5000);

        Assert.assertNotNull("It's right", stopTest);
        sAbilityDelegator.stopAbility(ability);
    }
}