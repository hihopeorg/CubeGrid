package com.lighters.cubegriddemo.slice;

import com.lighters.cubegriddemo.ResourceTable;
import com.lighters.cubegridlibrary.callback.ICubeGridAnimCallback;
import com.lighters.cubegridlibrary.view.CubeGridFrameLayout;
import com.lighters.cubegridlibrary.view.CubeGridImageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class MainAbilitySlice extends AbilitySlice {
    private CubeGridImageView mCubeGridImageView;
    private Button mBtnStart1;
    private Button mBtnStop1;
    private CubeGridFrameLayout mCubeGridFrameLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        mCubeGridImageView = (CubeGridImageView) findComponentById(ResourceTable.Id_iv_cube_grid);
        mCubeGridFrameLayout = (CubeGridFrameLayout) findComponentById(ResourceTable.Id_fl_cube_grid);
        mBtnStart1 = (Button) findComponentById(ResourceTable.Id_btn_grid_start1);
        mBtnStop1 = (Button) findComponentById(ResourceTable.Id_btn_grid_stop1);
        mBtnStart1.setClickedListener(mOnClickListener);
        mBtnStop1.setClickedListener(mOnClickListener);
        mCubeGridImageView.start(mImageAnimCallback);

        new EventHandler(EventRunner.create()).postTask(new Runnable() {
            @Override
            public void run() {
                mCubeGridFrameLayout.start(mFrameLayoutAnimCallback);
            }
        }, 1000);
    }

    private ICubeGridAnimCallback mImageAnimCallback = new ICubeGridAnimCallback() {
        @Override
        public void onAnimStart() {

        }

        @Override
        public void onAnimExecute(int curLoopCount) {
            LogUtils.info("image loop:" + curLoopCount);
        }

        @Override
        public void onAnimEnd() {
            new ToastDialog(MainAbilitySlice.this).setText("ImageAnimEnd").show();
        }
    };

    private ICubeGridAnimCallback mFrameLayoutAnimCallback = new ICubeGridAnimCallback() {
        @Override
        public void onAnimStart() {

        }

        @Override
        public void onAnimExecute(int curLoopCount) {
            LogUtils.info("frame layout loop:" + curLoopCount);
        }

        @Override
        public void onAnimEnd() {
            new ToastDialog(MainAbilitySlice.this).setText("FrameAnimEnd").show();
        }
    };
    private Component.ClickedListener mOnClickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            switch (component.getId()) {
                case ResourceTable.Id_btn_grid_start1:
                    mCubeGridImageView.start(mImageAnimCallback);
                    break;
                case ResourceTable.Id_btn_grid_stop1:
                    mCubeGridImageView.stop();
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        mCubeGridImageView.destory();
        mCubeGridFrameLayout.destroy();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
