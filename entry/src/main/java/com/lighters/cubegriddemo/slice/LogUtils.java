package com.lighters.cubegriddemo.slice;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Arrays;
import java.util.Iterator;

/**
 * <pre>
 *    author : Senh Linsh
 *    github : https://github.com/SenhLinsh
 *    date   : 2017/11/10
 *    desc   : 工具类: 打印相关
 * </pre>
 */
public class LogUtils {

    private LogUtils() {
    }

    /**
     * 打印时的 TAG
     */
    private static String sTag = LogUtils.class.getSimpleName() + ": ";

    /**
     * 类名
     */
    static String className;

    /**
     * 方法名
     */
    static String methodName;

    /**
     * 行数
     */
    static int lineNumber;

    static int CLASS_DOMAIN = 0x00201;
    static int CLASS_TYPE = HiLog.LOG_APP;
    private static HiLogLabel hiLogLabel;

    /**
     * 获取文件名、方法名、所在行数
     */
    private static void getMethodNames(StackTraceElement[] sElements) {
        className = sElements[1].getFileName();
        methodName = sElements[1].getMethodName();
        lineNumber = sElements[1].getLineNumber();
        hiLogLabel = new HiLogLabel(CLASS_TYPE, CLASS_DOMAIN, className);
    }

    public static void debug(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.debug(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void info(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.info(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void error(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.error(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void fatal(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.fatal(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void warn(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.warn(hiLogLabel, " %{public}s", createLog(message));
    }

    /**
     * 获取可抛异常对象的堆栈信息
     *
     * @param thr 可抛异常
     * @return 堆栈信息
     */
    public static String getStackTraceString(Throwable thr) {

        return HiLog.getStackTrace(thr);
    }

    /**
     * 获取调用 Log 方法的类名
     */
    private static String getClassName() {
        StackTraceElement thisMethodStack = (new Exception()).getStackTrace()[2];
        String result = thisMethodStack.getClassName();
        String[] split = result.split("\\.");
        try {
            result = split[split.length - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String toString(Object obj) {
        if (obj == null) return "null";
        if (obj instanceof int[]) {
            return Arrays.toString((int[]) obj);
        } else if (obj instanceof long[]) {
            return Arrays.toString((long[]) obj);
        } else if (obj instanceof short[]) {
            return Arrays.toString((short[]) obj);
        } else if (obj instanceof char[]) {
            return Arrays.toString((char[]) obj);
        } else if (obj instanceof byte[]) {
            return Arrays.toString((byte[]) obj);
        } else if (obj instanceof boolean[]) {
            return Arrays.toString((boolean[]) obj);
        } else if (obj instanceof float[]) {
            return Arrays.toString((float[]) obj);
        } else if (obj instanceof double[]) {
            return Arrays.toString((double[]) obj);
        } else if (obj instanceof Object[]) {
            return Arrays.toString((Object[]) obj);
        } else if (obj instanceof Iterable) {
            return toString((Iterable) obj);
        } else {
            return obj.toString();
        }
    }

    private static String toString(Iterable iterable) {
        Iterator iterator = iterable.iterator();
        if (!iterator.hasNext()) return "[]";
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (; ; ) {
            Object element = iterator.next();
            builder.append(element);
            if (!iterator.hasNext())
                return builder.append(']').toString();
            builder.append(',').append(' ');
        }
    }

    /**
     * type：用于指定输出日志的类型。HiLog中当前只提供了一种日志类型，即应用日志类型LOG_APP。
     * domain：用于指定输出日志所对应的业务领域，取值范围为0x0~0xFFFFF，开发者可以根据需要进行自定义。
     * tag：用于指定日志标识，可以为任意字符串，建议标识调用所在的类或者业务行为。
     */
    private static String createLog(String log) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("(")
                .append(methodName)
                .append(":" + lineNumber)
                .append(")");
        buffer.append(log);
        return buffer.toString();
    }
}
