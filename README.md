# CubeGrid

**本项目是基于开源项目CubeGrid进行ohos的移植和开发的，可以通过项目标签以及github地址(https://github.com/alighters/CubeGrid  )追踪到原安卓项目版本**

#### 项目介绍

- 项目名称：CubeGrid
- 所属系列：ohos第三方组件适配移植
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/alighters/CubeGrid
- 原项目基线版本：v1.2.5 ,sha1: commit 1ddc9256e74ecc506e06ecce51499d080e1926b5
- 功能介绍：ohos的立方体网格动画
- 编程语言：Java
- 外部依赖：无

## 效果展示：

<img src="screenshot/效果展示.gif"/>

#### 安装教程

方案一
1. 编译依赖的har包CubeGrid.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方案二
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.lighters.cubegridlibrary.ohos:CubeGrid:1.0.0'
}
```
#### 使用说明

### Step 1
```
    <com.lighters.cubegridlibrary.view.CubeGridImageView
        ohos:id="$+id:iv_cube_grid"
        ohos:height="100vp"
        ohos:width="100vp"
        ohos:image_src="$media:icon"
        ohos:padding="12vp"
        app:fillColor="#f00"
        app:loopCount="3"
        app:roundCornerSize="6dp"
        />

    <com.lighters.cubegridlibrary.view.CubeGridFrameLayout
        ohos:id="$+id:fl_cube_grid"
        ohos:height="80vp"
        ohos:width="80vp"
        ohos:top_margin="100vp"
        app:fillColor="#0f0"
        app:loopCount="10"
        app:roundCornerSize="6dp"/>
```
### Step 2
```
public class MainAbilitySlice extends AbilitySlice {
    private CubeGridImageView mCubeGridImageView;
    private Button mBtnStart1;
    private Button mBtnStop1;
    private CubeGridFrameLayout mCubeGridFrameLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        mCubeGridImageView = (CubeGridImageView) findComponentById(ResourceTable.Id_iv_cube_grid);
        mCubeGridFrameLayout = (CubeGridFrameLayout) findComponentById(ResourceTable.Id_fl_cube_grid);
        mBtnStart1 = (Button) findComponentById(ResourceTable.Id_btn_grid_start1);
        mBtnStop1 = (Button) findComponentById(ResourceTable.Id_btn_grid_stop1);
        mBtnStart1.setClickedListener(mOnClickListener);
        mBtnStop1.setClickedListener(mOnClickListener);
        mCubeGridImageView.start(mImageAnimCallback);

        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                mCubeGridFrameLayout.start(mFrameLayoutAnimCallback);
            }
        }, 100);
    }

    private ICubeGridAnimCallback mImageAnimCallback = new ICubeGridAnimCallback() {
        @Override
        public void onAnimStart() {

        }

        @Override
        public void onAnimExecute(int curLoopCount) {
            LogUtils.info("image loop:" + curLoopCount);
        }

        @Override
        public void onAnimEnd() {
            new ToastDialog(MainAbilitySlice.this).setText("ImageAnimEnd").show();
        }
    };

    private ICubeGridAnimCallback mFrameLayoutAnimCallback = new ICubeGridAnimCallback() {
        @Override
        public void onAnimStart() {

        }

        @Override
        public void onAnimExecute(int curLoopCount) {
            LogUtils.info("frame layout loop:" + curLoopCount);
        }

        @Override
        public void onAnimEnd() {
            new ToastDialog(MainAbilitySlice.this).setText("FrameAnimEnd").show();
        }
    };
    private Component.ClickedListener mOnClickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            switch (component.getId()) {
                case ResourceTable.Id_btn_grid_start1:
                    mCubeGridImageView.start(mImageAnimCallback);
                    break;
                case ResourceTable.Id_btn_grid_stop1:
                    mCubeGridImageView.stop();
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        mCubeGridImageView.destory();
        mCubeGridFrameLayout.destroy();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
```
#### 版本迭代
- v1.0.0
- 实现立体网格动画的启动与停止

#### 版权与协议
```
Copyright (C) 2016 david.wei (lighters)
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```